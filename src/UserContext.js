import React from 'react';

// Creates a React context object
// A context object contains data that can be passed around to multiple props
// Like a delivery box or gift
const UserContext = React.createContext();

// Provider is what is used to distribute the context object to the components or santa claus
export const UserProvider = UserContext.Provider;

export default UserContext;
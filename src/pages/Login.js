import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';


export default function Login() {
	
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	const { user, setUser } = useContext(UserContext);

	useEffect(() => {
		// console.log(email)
		// console.log(password1)
		// console.log(password2)

		// if all fields are populated and passwords match
		if(email !== '' && password !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	function loginUser(e){
		e.preventDefault()//prevent default form behavior so form does not submit

		// localStorage.setItem allows us to save a key/value pair to localStorage
		// Syntax: localStorage.setItem('key', value)
		localStorage.setItem('email', email)

		setUser({
			email: email
		})

		setEmail("")
		setPassword("")
		
		alert('Login Successful')
	}

	return (
		<>
		{
			(user.email !== null) ? 		
			<Redirect to="/"/>
			:
			<Redirect to="/login"/>
		}

		<Form onSubmit={e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Enter Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{
				isActive ? 	
					<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
					Login
					</Button>
					:
					<Button className="mt-3" variant="primary" id="submitBtn" disabled>
					Login
					</Button>	
			}
	
		</Form>
		</>
	)

}


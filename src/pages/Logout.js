import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	// get the setUser setter from App.js
	const { setUser } = useContext(UserContext);

	// clear localStorage
	localStorage.clear()

	// when component mounts/page loads, set the user state to null
	useEffect(() => {
		setUser({
			email: null
		})
	})

	return(
		// redirects user to login page
		<Redirect to="/login"/>
	)
}

// Homework Activity: Add a functionality to redirect the user back to the homepage if they are logged in and try to access the login or register pages
// Condition to check if user is logged in. and if email is not null, they will be redirected (user.email !== null)